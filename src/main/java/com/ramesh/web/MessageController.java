package com.ramesh.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public ResponseEntity<String> persistPerson(@RequestBody String text) {
		return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).build();
	}
}
